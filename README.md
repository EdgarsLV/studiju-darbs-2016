# Studiju darbs I

## Screenshots
![Main Window](https://bitbucket.org/neonuni/studiju-darbs-2016/raw/master/Screenshots/img01.png)
![Results](https://bitbucket.org/neonuni/studiju-darbs-2016/raw/master/Screenshots/img02.png)

## Running
* Open solution (.sln)
* Restore nuget (for BouncyCastle)
* Run _SdCrypto.SdGui_ project