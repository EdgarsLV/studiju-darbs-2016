﻿// Kursa darbs 2016
// https://bitbucket.org/neonuni/sd2016
namespace SdCrypto
{
    public interface ICryptWorker
    {
        string WorkerId { get; }
        string Name { get; }

        int KeySize { get; }
        int BlockSize { get; }
        string Mode { get; }

        string EncryptFile(string inPath, string outPath);
        string DecryptFile(string inPath, string outPath);

        byte[] EncryptBytes(byte[] bytes);
        byte[] DecryptBytes(byte[] bytes);
    }
}