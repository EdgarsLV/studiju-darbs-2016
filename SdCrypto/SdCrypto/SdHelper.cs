﻿// Kursa darbs 2016
// https://bitbucket.org/neonuni/sd2016
using System;
using System.IO;
using System.Security.Cryptography;

namespace SdCrypto
{
    public static class SdHelper
    {
        /// <summary>
        /// Creates an "empty" file of certain size
        /// </summary>
        /// <param name="filePath">Path where to create the file, including filename</param>
        /// <param name="sizeMb">A megabyte is assumed to have 1024kb</param>
        /// <param name="randomBytes">If true, bytes will be random, otherwise all bytes will be 0</param>
        public static void CreateEmptyFile(string filePath, long sizeMb, bool randomBytes = false)
        {
            if (randomBytes)
                CreateEmptyFileRandomBytes(filePath, sizeMb);
            else
                CreateEmptyFileZeroBytes(filePath, sizeMb);
        }
        private static void CreateEmptyFileRandomBytes(string filePath, long sizeMb)
        {
            using (var stream = File.OpenWrite(filePath))
            {
                var rnd = new Random();
                for (long i = 0; i < sizeMb*1024*1024; i++)
                {
                    stream.WriteByte((byte)rnd.Next(0, 254));
                }
            }

        }
        private static void CreateEmptyFileZeroBytes(string filePath, long sizeMb)
        {
            using (var stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                // sets file to be of size filled with nulls
                stream.SetLength(sizeMb * 1024 * 1024);
            }
        }

        /// <summary>
        /// Gets md5 checksum from a file
        /// </summary>
        /// <param name="filePath">File to sum</param>
        /// <returns>MD5 format checksum</returns>
        public static string GetChecksumMd5(string filePath)
        {
            using (var md5 = MD5.Create())
            using (var stream = File.OpenRead(filePath))
            {
                return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
            }
        }
        /// <summary>
        /// Gets md5 checksum from array of bytes
        /// </summary>
        /// <param name="bytes">Bytes to sum</param>
        /// <returns>MD5 format checksum</returns>
        public static string GetChecksumMd5(byte[] bytes)
        {
            using (var md5 = MD5.Create())
            {
                return BitConverter.ToString(md5.ComputeHash(bytes)).Replace("-", string.Empty);
            }
        }
    }
}