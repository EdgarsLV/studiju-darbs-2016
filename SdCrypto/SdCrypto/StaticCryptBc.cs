﻿// Kursa darbs 2016
// https://bitbucket.org/neonuni/sd2016
using System;
using System.IO;
using Org.BouncyCastle.Crypto.Paddings;

namespace SdCrypto
{
    public class StaticCryptBc
    {
        public static byte[] DoCryptBytes(byte[] bytes, PaddedBufferedBlockCipher cipher)
        {
            return cipher.DoFinal(bytes); // only 1 chunk so we can instantly do a final cipher
        }

        public static string DoCryptFile(string inPath, string outPath, PaddedBufferedBlockCipher cipher)
        {
            var inBuffer = new byte[1024];
            var outBuffer = new byte[1024];

            using(var fsO = new FileStream(outPath, FileMode.Create, FileAccess.Write))
            using (var fsI = new FileStream(inPath, FileMode.Open, FileAccess.Read))
            {
                int bRead;
                int bProcessed;

                while ((bRead = fsI.Read(inBuffer, 0, inBuffer.Length)) > 0)
                {
                    bProcessed = cipher.ProcessBytes(inBuffer, 0, bRead, outBuffer, 0);
                    fsO.Write(outBuffer, 0, bProcessed);
                }
                bProcessed = cipher.DoFinal(outBuffer, 0);
                fsO.Write(outBuffer, 0, bProcessed);
            }

            if (File.Exists(outPath)) return outPath;
            throw new Exception("Failed to crypt file for some reason");
        }
    }
}
