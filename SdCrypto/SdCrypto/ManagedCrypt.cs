﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Linq;
namespace SdCrypto
{
    /// <summary>
    /// dotNET crypto class
    /// </summary>
    public class ManagedCrypt : ICryptWorker
    {
        public string WorkerId { get; }
        public string Name { get; }
        public int KeySize { get; }
        public int BlockSize { get; }
        public string Mode { get; }

        public ManagedCrypt(SymmetricAlgorithm algo, int keySize = -1, int blockSize = -1, string id = "", string name = "", CipherMode mode = CipherMode.CBC, string password = "password")
        {
            KeySize = keySize < 0 ? algo.LegalKeySizes[0].MaxSize : keySize;
            BlockSize = blockSize < 0 ? algo.LegalBlockSizes[0].MaxSize : blockSize;
            Mode = mode.ToString();

            if (string.IsNullOrWhiteSpace(id))
            {
                var n = algo.ToString().Split('.').Last() + KeySize;
                n = n.Replace("CryptoServiceProvider", "Cps");
                WorkerId = n;
            }
            else WorkerId = id;

            if (string.IsNullOrWhiteSpace(name))
            {
                var n = algo.ToString().Split('.').Last() + " " + KeySize + "/" + BlockSize;
                n = n.Replace("CryptoServiceProvider", "Cps");
                Name = n;
            }
            else Name = name;

            var salt = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 }; // salt is kept unchanged for test reasons
            var keyGen = new Rfc2898DeriveBytes(password, salt);
            algo.KeySize = KeySize;
            algo.BlockSize = BlockSize;
            algo.Key = keyGen.GetBytes(algo.KeySize / 8);
            algo.IV = keyGen.GetBytes(algo.BlockSize / 8);
            _algo = algo;
        }


        private readonly SymmetricAlgorithm _algo;

        public string EncryptFile(string inPath, string outPath)
        {
            return DoCryptFile(inPath, outPath, _algo.CreateEncryptor());
        }

        public string DecryptFile(string inPath, string outPath)
        {
            return DoCryptFile(inPath, outPath, _algo.CreateDecryptor());
        }

        public byte[] EncryptBytes(byte[] bytes)
        {
            return DoCryptBytes(bytes, _algo.CreateEncryptor());
        }

        public byte[] DecryptBytes(byte[] bytes)
        {
            return DoCryptBytes(bytes, _algo.CreateDecryptor());
        }

        private static byte[] DoCryptBytes(byte[] bytes, ICryptoTransform transform)
        {
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, transform, CryptoStreamMode.Write))
                    cs.Write(bytes, 0, bytes.Length);
                return ms.ToArray();
            }
        }
        private static string DoCryptFile(string inPath, string outPath, ICryptoTransform transform)
        {
            using (var cryptoTransform = transform)
            using (var fsOut = new FileStream(outPath, FileMode.Create, FileAccess.Write))
            using (var fsIn = new FileStream(inPath, FileMode.Open, FileAccess.Read))
            using (var cs = new CryptoStream(fsOut, cryptoTransform, CryptoStreamMode.Write))
            {
                var buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = fsIn.Read(buffer, 0, buffer.Length)) != 0)
                    cs.Write(buffer, 0, bytesRead);
            }

            if (File.Exists(outPath)) return outPath;
            throw new Exception("Failed to crypt file for some reason");
        }

        public static List<ManagedCrypt> GetAllWorkerVariations()
        {
            return new List<ManagedCrypt>
            {
                new ManagedCrypt(new DESCryptoServiceProvider(), 64),

                new ManagedCrypt(new TripleDESCryptoServiceProvider(), 128),
                new ManagedCrypt(new TripleDESCryptoServiceProvider(), 192),

                new ManagedCrypt(new AesCryptoServiceProvider(), 128),
                new ManagedCrypt(new AesCryptoServiceProvider(), 192),
                new ManagedCrypt(new AesCryptoServiceProvider(), 256),

                new ManagedCrypt(new AesManaged(), 128),
                new ManagedCrypt(new AesManaged(), 192),
                new ManagedCrypt(new AesManaged(), 256),

                new ManagedCrypt(new RijndaelManaged(), 128, 128),
                new ManagedCrypt(new RijndaelManaged(), 192, 128),
                new ManagedCrypt(new RijndaelManaged(), 256, 128),
            };
        }

    }
}