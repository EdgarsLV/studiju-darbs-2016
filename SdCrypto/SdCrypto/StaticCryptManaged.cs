﻿// Kursa darbs 2016
// https://bitbucket.org/neonuni/sd2016
using System;
using System.IO;
using System.Security.Cryptography;

namespace SdCrypto
{
    public static class StaticCryptManaged
    {
        public static byte[] DoCryptBytes(byte[] bytes, ICryptoTransform transform)
        {
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, transform, CryptoStreamMode.Write))
                    cs.Write(bytes, 0, bytes.Length);
                return ms.ToArray();
            }
        }

        public static string DoCryptFile(string inPath, string outPath, ICryptoTransform transform)
        {
            using (var cryptoTransform = transform)
            using (var fsOut = new FileStream(outPath, FileMode.Create, FileAccess.Write))
            using (var fsIn = new FileStream(inPath, FileMode.Open, FileAccess.Read))
            using (var cs = new CryptoStream(fsOut, cryptoTransform, CryptoStreamMode.Write))
            {
                var buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = fsIn.Read(buffer, 0, buffer.Length)) != 0)
                    cs.Write(buffer, 0, bytesRead);
            }

            if (File.Exists(outPath)) return outPath;
            throw new Exception("Failed to crypt file for some reason");
        }
    }
}