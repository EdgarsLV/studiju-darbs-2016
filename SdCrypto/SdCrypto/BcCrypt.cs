﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Paddings;
using Org.BouncyCastle.Crypto.Modes;

namespace SdCrypto
{
    /// <summary>
    /// Bouncy Castle crypto class
    /// </summary>
    public class BcCrypt : ICryptWorker
    {
        public string WorkerId { get; }
        public string Name { get; }
        public int KeySize { get; }
        public int BlockSize { get; }
        public string Mode { get; }


        public BcCrypt(IBlockCipher algo, int keySize, string id = "", string name = "", string password = "password")
        {
            KeySize = keySize;
            BlockSize = algo.GetBlockSize() * 8;
            
            Mode = "CBC";
            if (string.IsNullOrWhiteSpace(id))
            {
                var n = algo.ToString().Split('.').Last() + KeySize;
                n = n.Replace("Engine", "BC");
                WorkerId = n;
            }
            else WorkerId = id;
            
            if (string.IsNullOrWhiteSpace(name))
            {
                var n = algo.ToString().Split('.').Last() + " " + KeySize + "/" + BlockSize;
                n = n.Replace("Engine", "BC");
                Name = n;
            }
            else Name = name;
 
            var salt = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 }; // salt is kept unchanged for test reasons
            var keyGen = new Rfc2898DeriveBytes(password, salt);

            var key = keyGen.GetBytes(keySize / 8);
            var kp = new KeyParameter(key);
            
            // note - encryptor and decryptor must derive from separate cipher instances
            _pbEnc = new PaddedBufferedBlockCipher(new CbcBlockCipher(Activator.CreateInstance(algo.GetType()) as IBlockCipher));
            _pbDec = new PaddedBufferedBlockCipher(new CbcBlockCipher(Activator.CreateInstance(algo.GetType()) as IBlockCipher));
            _pbEnc.Init(true, kp);
            _pbDec.Init(false, kp);
        }

        private readonly PaddedBufferedBlockCipher _pbEnc;
        private readonly PaddedBufferedBlockCipher _pbDec;


        public string EncryptFile(string inPath, string outPath)
        {
            return DoCryptFile(inPath, outPath, _pbEnc);
        }

        public string DecryptFile(string inPath, string outPath)
        {
            return DoCryptFile(inPath, outPath, _pbDec);
        }

        public byte[] EncryptBytes(byte[] bytes)
        {
            return DoCryptBytes(bytes, _pbEnc);
        }

        public byte[] DecryptBytes(byte[] bytes)
        {
            return DoCryptBytes(bytes, _pbDec);
        }

        private static string DoCryptFile(string inPath, string outPath, IBufferedCipher cipher)
        {
            var inBuffer = new byte[1024];
            var outBuffer = new byte[1024];

            using (var fsO = new FileStream(outPath, FileMode.Create, FileAccess.Write))
            using (var fsI = new FileStream(inPath, FileMode.Open, FileAccess.Read))
            {
                int bRead;
                int bProcessed;

                while ((bRead = fsI.Read(inBuffer, 0, inBuffer.Length)) > 0)
                {
                    bProcessed = cipher.ProcessBytes(inBuffer, 0, bRead, outBuffer, 0);
                    fsO.Write(outBuffer, 0, bProcessed);
                }
                bProcessed = cipher.DoFinal(outBuffer, 0);
                fsO.Write(outBuffer, 0, bProcessed);
            }

            if (File.Exists(outPath)) return outPath;
            throw new Exception("Failed to crypt file for some reason");
        }
        private static byte[] DoCryptBytes(byte[] bytes, IBufferedCipher cipher)
        {
            return cipher.DoFinal(bytes); // only 1 chunk so we can instantly do a final cipher
        }

        public static List<BcCrypt> GetAllWorkerVariations()
        {
            return new List<BcCrypt>
            {
                new BcCrypt(new AesEngine(), 128),
                new BcCrypt(new AesEngine(), 192),
                new BcCrypt(new AesEngine(), 256),

                new BcCrypt(new BlowfishEngine(), 128),
                new BcCrypt(new BlowfishEngine(), 192),
                new BcCrypt(new BlowfishEngine(), 256),

                new BcCrypt(new TwofishEngine(), 128),
                new BcCrypt(new TwofishEngine(), 192),
                new BcCrypt(new TwofishEngine(), 256),

                new BcCrypt(new SerpentEngine(), 128),
                new BcCrypt(new SerpentEngine(), 192),
                new BcCrypt(new SerpentEngine(), 256),
            };
        }
    }
}