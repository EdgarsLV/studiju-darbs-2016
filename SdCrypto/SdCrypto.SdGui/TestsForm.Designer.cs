﻿namespace SdCrypto.SdGui
{
    partial class TestsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRunTest = new System.Windows.Forms.Button();
            this.dgTestResults = new System.Windows.Forms.DataGridView();
            this.lTestsPerformedText = new System.Windows.Forms.Label();
            this.lPerformingTests = new System.Windows.Forms.Label();
            this.lTestsPerformedNum = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgTestResults)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRunTest
            // 
            this.btnRunTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRunTest.Location = new System.Drawing.Point(12, 344);
            this.btnRunTest.Name = "btnRunTest";
            this.btnRunTest.Size = new System.Drawing.Size(75, 23);
            this.btnRunTest.TabIndex = 1;
            this.btnRunTest.Text = "Run Test";
            this.btnRunTest.UseVisualStyleBackColor = true;
            this.btnRunTest.Click += new System.EventHandler(this.btnRunTest_Click);
            // 
            // dgTestResults
            // 
            this.dgTestResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgTestResults.BackgroundColor = System.Drawing.SystemColors.Menu;
            this.dgTestResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgTestResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTestResults.Location = new System.Drawing.Point(12, 12);
            this.dgTestResults.Name = "dgTestResults";
            this.dgTestResults.Size = new System.Drawing.Size(1058, 313);
            this.dgTestResults.TabIndex = 2;
            // 
            // lTestsPerformedText
            // 
            this.lTestsPerformedText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lTestsPerformedText.AutoSize = true;
            this.lTestsPerformedText.Location = new System.Drawing.Point(9, 328);
            this.lTestsPerformedText.Name = "lTestsPerformedText";
            this.lTestsPerformedText.Size = new System.Drawing.Size(89, 13);
            this.lTestsPerformedText.TabIndex = 3;
            this.lTestsPerformedText.Text = "Tests performed: ";
            // 
            // lPerformingTests
            // 
            this.lPerformingTests.AutoSize = true;
            this.lPerformingTests.BackColor = System.Drawing.Color.Transparent;
            this.lPerformingTests.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lPerformingTests.Location = new System.Drawing.Point(14, 14);
            this.lPerformingTests.Name = "lPerformingTests";
            this.lPerformingTests.Size = new System.Drawing.Size(181, 13);
            this.lPerformingTests.TabIndex = 4;
            this.lPerformingTests.Text = "Performing tests, this will take a while";
            // 
            // lTestsPerformedNum
            // 
            this.lTestsPerformedNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lTestsPerformedNum.AutoSize = true;
            this.lTestsPerformedNum.Location = new System.Drawing.Point(90, 328);
            this.lTestsPerformedNum.Name = "lTestsPerformedNum";
            this.lTestsPerformedNum.Size = new System.Drawing.Size(46, 13);
            this.lTestsPerformedNum.TabIndex = 5;
            this.lTestsPerformedNum.Text = "{0} / {1}";
            // 
            // TestsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1082, 379);
            this.Controls.Add(this.lTestsPerformedNum);
            this.Controls.Add(this.lPerformingTests);
            this.Controls.Add(this.lTestsPerformedText);
            this.Controls.Add(this.dgTestResults);
            this.Controls.Add(this.btnRunTest);
            this.Name = "TestsForm";
            this.Text = "Test Results";
            ((System.ComponentModel.ISupportInitialize)(this.dgTestResults)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnRunTest;
        private System.Windows.Forms.DataGridView dgTestResults;
        private System.Windows.Forms.Label lTestsPerformedText;
        private System.Windows.Forms.Label lPerformingTests;
        private System.Windows.Forms.Label lTestsPerformedNum;
    }
}