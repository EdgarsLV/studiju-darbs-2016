﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SdCrypto.SdGui
{
    public partial class TestsForm : Form
    {
        public delegate void MakeListDelegate();
        public TestsForm(List<ICryptWorker> workers, List<string> files, int iterations, bool inMemoryTest = false)
        {
            InitializeComponent();
            _workers = workers;
            _iterations = iterations;
            _files = files;
            _inMemoryTest = inMemoryTest;
            _totalTests = workers.Count;
            IncrementCurrentTests();
            RunTests();
        }

        private List<TestResult> _results = new List<TestResult>();
        private readonly List<ICryptWorker> _workers;
        private readonly List<string> _files;
        private readonly int _iterations;
        private Timer _timer;
        private bool _testsRunning;
        private readonly bool _inMemoryTest;
        private int _currentTests = -1;
        private readonly int _totalTests;

        private void RunTests()
        {
            if (_testsRunning) return;
            _testsRunning = true;
            lPerformingTests.Text = lPerformingTests.Text.Replace(".","");
            lPerformingTests.Show();
            _timer = new Timer { Interval = 100 };
            _timer.Tick += OnTimerTick;
            _timer.Start();
            Task.Run(() =>
            {
                _results = RunTests(_workers, _files, _iterations, _inMemoryTest, IncrementCurrentTests, this);
                Invoke(new MakeListDelegate(MakeList));
            });
        }

        private void IncrementCurrentTests()
        {
            if (_currentTests+1 > _totalTests) _currentTests = 0;
            _currentTests++;
            lTestsPerformedNum.Text = $"{_currentTests}/{_totalTests}";
        }

        private void MakeList()
        {
            lPerformingTests.Hide();
            _timer.Stop();
            _testsRunning = false;

            var dt = new DataTable();

            dt.Columns.AddRange(new[] {
                                new DataColumn("Id"),
                                new DataColumn("Algo"),
                                new DataColumn("Md5", typeof(bool)),
                                new DataColumn("File"),
                                new DataColumn("Size (mb)"),

                                //new DataColumn("Enc(ms)\nMin"),
                                //new DataColumn("Enc(ms)\nMax"),
                                new DataColumn("Enc(ms)\nAvg"),

                                //new DataColumn("Dec(ms)\nMin"),
                                //new DataColumn("Dec(ms)\nMax"),
                                new DataColumn("Dec(ms)\nAvg"),

                                new DataColumn("Enc (mb/s)"),
                                new DataColumn("Dec (mb/s)"),

                                new DataColumn("Iterations"),
            });
            var id = 0;

            //TestResult.SaveToFile($"{App.TestEnvDynPath}/{DateTime.Now.ToString("HH-mm-ss")}.txt", _results);

            _results.ForEach(x =>
            {
                dt.Rows.Add(
                    id,
                    x.CryptWorker.Name,
                    x.MdMatch,
                    x.TargetFileInfo.Name,
                    Math.Round((double)x.TargetFileInfo.Length / 1024 / 1024,2),

                    //x.TimeEncryptShortest,
                    //x.TimeEncryptLongest,
                    x.TimeEncryptAverage,

                    //x.TimeDecryptShortest,
                    //x.TimeDecryptLongest,
                    x.TimeDecryptAverage,

                    Math.Round(x.TargetFileInfo.Length / ((double)x.TimeEncryp / 1000)/1024/1024, 2),
                    Math.Round(x.TargetFileInfo.Length / ((double)x.TimeDecrypt / 1000)/1024/1024, 2),
                    x.Iterations
                    );
                id++;
            });

            dgTestResults.DataSource = dt;
            dgTestResults.AutoResizeColumns();
            dgTestResults.ReadOnly = true;

            //lTestsPerformedText.Text = $"Tests performed: {_results.Count}";
        }

        private static List<TestResult> RunTests(IEnumerable<ICryptWorker> workers, List<string> files, int iterations, bool inMemory, MakeListDelegate makeListDelegate = null, Control control = null)
        {
            var list = new List<TestResult>();
            foreach (var w in workers)
            {
                foreach (var f in files)
                {
                    list.Add(TestResult.PerformTest(w, f, iterations, inMemory));
                    if (makeListDelegate != null) control?.Invoke(makeListDelegate);
                }
            }

            return list;
        }

        private void OnTimerTick(object o, EventArgs e)
        {
            lPerformingTests.Text += "=";
            if (lPerformingTests.Text.Length > 64)
                lPerformingTests.Text = "";
        }

        private void btnRunTest_Click(object sender, EventArgs e)
        {
            RunTests();
        }
    }
}
