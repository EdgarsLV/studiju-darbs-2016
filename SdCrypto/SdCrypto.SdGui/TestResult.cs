﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace SdCrypto.SdGui
{
    public class TestResult
    {
        public long TimeEncryp;
        public long TimeDecrypt;

        public long TimeEncryptLongest;
        public long TimeEncryptShortest;
        public double TimeEncryptAverage;

        public long TimeDecryptLongest;
        public long TimeDecryptShortest;
        public double TimeDecryptAverage;

        public int Iterations;

        public bool InMemory;

        public FileInfo TargetFileInfo;
        public ICryptWorker CryptWorker;
        public bool MdMatch;

        /// <summary>
        /// Performs a test that encrypts and decrypts a file and outputs a result with relevant info
        /// </summary>
        /// <param name="cryptWorker">The worker that will do the job</param>
        /// <param name="filePath">Path of the file to work with</param>
        /// <param name="iterations">How many times to perform the tests</param>
        /// <param name="inMemory">Is test performed in memory or by streaming file out to drive</param>
        /// <param name="useWarmupRound">If true, additional test will be run but removed so that it can warm up the system</param>
        /// <returns></returns>
        public static TestResult PerformTest(ICryptWorker cryptWorker, string filePath, int iterations = 1, bool inMemory = false, bool useWarmupRound = true)
        {
            if (iterations < 1) iterations = 1;
            if (useWarmupRound) iterations++;
            var tests = new List<TestResult>();

            var bytes = inMemory ? File.ReadAllBytes(filePath) : null;

            for (var i = 0; i < iterations; i++)
            {
                tests.Add(!inMemory
                    ? PerformSingleTest(cryptWorker, filePath)
                    : PerformSingleInMemoryTest(cryptWorker, bytes));
            }
            if (useWarmupRound)
                tests.RemoveAt(0); //remove warmup result as its slower than others

            var tr = new TestResult
            {
                TimeEncryp = tests.Select(x => x.TimeEncryp).Min(),
                TimeEncryptShortest = tests.Select(x => x.TimeEncryp).Min(),
                TimeEncryptAverage   = tests.Select(x => x.TimeEncryp).Average(),
                TimeEncryptLongest  = tests.Select(x => x.TimeEncryp).Max(),

                TimeDecrypt = tests.Select(x => x.TimeDecrypt).Min(),
                TimeDecryptShortest = tests.Select(x => x.TimeDecrypt).Min(),
                TimeDecryptAverage   = tests.Select(x => x.TimeDecrypt).Average(),
                TimeDecryptLongest   = tests.Select(x => x.TimeDecrypt).Max(),

                TargetFileInfo  = new FileInfo(filePath),
                InMemory        = inMemory,
                CryptWorker     = cryptWorker,
                MdMatch         = tests.Count(x => x.MdMatch == false) <= 0, //if at least 1 md fails, assume failed check //tests[0].MdMatch,
                Iterations      = useWarmupRound ? iterations-1 : iterations
            };
            return tr;
        }

        private static TestResult PerformSingleTest(ICryptWorker cryptWorker, string filePath)
        {
            var tr = new TestResult();
            var sw = new Stopwatch();
            sw.Reset();
            tr.TargetFileInfo = new FileInfo(filePath);
            tr.CryptWorker = cryptWorker;

            var fileMd5 = SdHelper.GetChecksumMd5(filePath);
            var cryPath = $"{App.TestEnvDynPath}/{tr.TargetFileInfo.Name}.{cryptWorker.WorkerId}";
            var decryPath = $"{App.TestEnvDynPath}/{tr.TargetFileInfo.Name}.{cryptWorker.WorkerId}{tr.TargetFileInfo.Extension}";

            sw.Start();
            cryptWorker.EncryptFile(filePath, cryPath);
            sw.Stop();

            tr.TimeEncryp = sw.ElapsedMilliseconds;
            sw.Reset();

            sw.Start();
            cryptWorker.DecryptFile(cryPath, decryPath);
            sw.Stop();

            tr.TimeDecrypt = sw.ElapsedMilliseconds;
            sw.Reset();

            var nfileMd5 = SdHelper.GetChecksumMd5(decryPath);
            if (fileMd5 == nfileMd5) tr.MdMatch = true;

            return tr;
        }

        private static TestResult PerformSingleInMemoryTest(ICryptWorker worker, byte[] bytes)
        {
            var tr = new TestResult();
            var sw = new Stopwatch();
            sw.Reset();
            tr.TargetFileInfo = null;
            tr.CryptWorker = worker;
            tr.InMemory = true;

            var bytesMd5 = SdHelper.GetChecksumMd5(bytes);
            byte[] cryBytes;

            sw.Start();
            cryBytes = worker.EncryptBytes(bytes);
            sw.Stop();
            tr.TimeEncryp = sw.ElapsedMilliseconds;
            sw.Reset();

            sw.Start();
            cryBytes = worker.DecryptBytes(cryBytes);
            sw.Stop();
            tr.TimeDecrypt = sw.ElapsedMilliseconds;

            var newBytesMd5 = SdHelper.GetChecksumMd5(cryBytes);
            if (bytesMd5 == newBytesMd5) tr.MdMatch = true;

            return tr;
        }

        public static void SaveToFile(string path, List<TestResult> results)
        {
            var list = new List<string>();
            foreach (var r in results)
            {
                list.Add($"{r.CryptWorker.WorkerId} {r.TimeEncryptAverage} {r.TimeDecryptAverage}");
            }
            var sb = new StringBuilder();
            list.ForEach(x => sb.AppendLine(x));
            File.WriteAllText(path, sb.ToString());
        }
    }
}