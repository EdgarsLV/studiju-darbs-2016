﻿// Kursa darbs 2016
// https://bitbucket.org/neonuni/sd2016
using System.Collections.Generic;
using System.IO;
namespace SdCrypto.SdGui
{
    public static class App
    {
        /// <summary>
        /// Folder where files to be encrypted are stored
        /// </summary>
        public const string TestEnvPath = "Testing";
        /// <summary>
        /// Folder where encrypted and decrypted resulting files are stored
        /// </summary>
        public const string TestEnvDynPath = "Testing/Crypt";

        static App()
        {
            if (!Directory.Exists(TestEnvPath)) Directory.CreateDirectory(TestEnvPath);
            if (!Directory.Exists(TestEnvDynPath)) Directory.CreateDirectory(TestEnvDynPath);
            CryptWorkers.AddRange(ManagedCrypt.GetAllWorkerVariations());
            CryptWorkers.AddRange(BcCrypt.GetAllWorkerVariations());
        }

        public static readonly List<string> PredefinedTestFiles = new List<string>
        {
            // files follow a name.size.bytes.ext format
            // name - name of the file
            // size - size in megabytes, this will be used to generate a file of such size
            // bytes - 0 = all bytes in file will be 0, 1 = bytes in file will be random

            //"file.1.0.blob",
            //"file.128.0.blob",
            //"file.256.0.blob",
            //"file.512.0.blob",
            //"file.1024.0.blob",

            //"file.1.1.blob",
            "file.64.1.blob",
            "file.128.1.blob",
            "file.256.1.blob",
            //"file.512.1.blob",
            //"file.1024.1.blob",
        };

        public static readonly List<ICryptWorker> CryptWorkers = new List<ICryptWorker>();

        public static void SetupTestEnvironment()
        {
            if (!Directory.Exists(TestEnvPath)) Directory.CreateDirectory(TestEnvPath);
            if (!Directory.Exists(TestEnvDynPath)) Directory.CreateDirectory(TestEnvDynPath);
            foreach (var f in PredefinedTestFiles)
            {
                var path = $"{TestEnvPath}/{f}";
                if (File.Exists(path)) continue; // no need to create a file that already exists
                var data = f.Split('.'); // split the name by dots
                var sizeMb = int.Parse(data[1]); // size is defined as 2nd item in format array
                var zeroFile = data[2] == "0"; // bytes are defined as 3rd item in format

                SdHelper.CreateEmptyFile(path, sizeMb, !zeroFile);
            }
        }

    }
}