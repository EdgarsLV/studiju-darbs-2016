﻿// Kursa darbs 2016
// https://bitbucket.org/neonuni/sd2016
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SdCrypto.SdGui
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            InitCryptoAlgosList();
            InitFilesList();
        }

        private void InitCryptoAlgosList()
        {
            lbCryptoAlgos.DataSource = App.CryptWorkers;
            lbCryptoAlgos.SelectionMode = SelectionMode.MultiExtended;
            lbCryptoAlgos.DrawMode = DrawMode.OwnerDrawFixed;
            for (var i = 0; i < App.CryptWorkers.Count; i++) // loop over all items and select them
            {
                lbCryptoAlgos.SetSelected(i, true);
            }
        }

        private void InitFilesList()
        {
            if (!Directory.Exists(App.TestEnvPath)) return;
            var files = new List<string>(); // files we can perform tests on
            Directory.GetFiles(App.TestEnvPath).ToList().ForEach(x => files.Add(x)); // search tests folder for files
            lbFiles.DataSource = files;
            lbFiles.SelectionMode = SelectionMode.MultiExtended;
            for (var i = 0; i < files.Count; i++) // loop over all items and select them
            {
                lbFiles.SetSelected(i, true);
            }
        }

        private void btnDoStandardTest_Click(object sender, EventArgs e)
        {
            if (lbCryptoAlgos.SelectedItems.Count == 0)
            {
                MessageBox.Show("No crypto selected");
                return;
            }
            if (lbFiles.SelectedItems.Count == 0)
            {
                MessageBox.Show("No files selected");
                return;
            }
            new TestsForm(
                        lbCryptoAlgos.SelectedItems.Cast<ICryptWorker>().ToList(),
                        lbFiles.SelectedItems.Cast<string>().ToList(),
                        (int)nIterations.Value,
                        chkInMemory.Checked
                        ).Show();
        }

        // called on each item selected in the algo list, used to set selection(background) color to green
        private void lbCryptoAlgos_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;
            var s = false;
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
            {
                e = new DrawItemEventArgs(e.Graphics, e.Font, e.Bounds, e.Index, e.State ^ DrawItemState.Selected,
                    e.ForeColor, Color.DarkCyan);
                s = true;
            }

            e.DrawBackground();
            e.Graphics.DrawString(((ICryptWorker)lbCryptoAlgos.Items[e.Index]).Name, e.Font, s ? Brushes.White : Brushes.Black, e.Bounds, StringFormat.GenericDefault);
            e.DrawFocusRectangle();
            lSelectedCount.Text = lbCryptoAlgos.SelectedItems.Count.ToString();
        }

        private void btnOpenTestsFolder_Click(object sender, EventArgs e)
        {
            Process.Start(App.TestEnvPath);
        }

        private void btnDeleteTests_Click(object sender, EventArgs e)
        {
            foreach (var f in Directory.GetFiles(App.TestEnvDynPath))
                File.Delete(f);
        }

        private void btnInitPredefFiles_Click(object sender, EventArgs e)
        {
            new ThreadedLoadingForm(
                "Please hold on...",
                "Creating predefined files, please wait",
                App.SetupTestEnvironment,
                InitFilesList
                ).ShowDialog();
        }

        private void btnRefreshFileList_Click(object sender, EventArgs e)
        {
            InitFilesList();
        }
    }
}
