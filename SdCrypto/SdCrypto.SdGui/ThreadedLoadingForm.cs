﻿// Kursa darbs 2016
// https://bitbucket.org/neonuni/sd2016
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SdCrypto.SdGui
{
    public partial class ThreadedLoadingForm : Form
    {
        /// <summary>
        /// Creates a form that runs an action without locking
        /// </summary>
        /// <param name="title">Title of the window</param>
        /// <param name="message">Message to display during loading</param>
        /// <param name="action">Action to execute</param>
        /// <param name="onDone">Action to execute when previous one is done</param>
        public ThreadedLoadingForm(string title, string message, Action action, Action onDone = null)
        {
            InitializeComponent();
            Load += delegate { Text = title; };
            _task = new Task(action);
            _onDone = onDone;
            lMessage.Text = message;
            var timer = new Timer();
            timer.Interval = 200;
            timer.Tick += OnTick;
            timer.Start();
            _task.Start();
        }

        private readonly Action _onDone;
        private readonly Task _task;

        private readonly string[] _animation = {"-","\\","|","/"};
        private int _animStep;
        private bool _done;
        private void OnTick(object sender, EventArgs e)
        {
            lLoading.Text = _animation[_animStep];
            _animStep = _animStep == _animation.Length-1 ? 0 : _animStep + 1;

            if (!_task.IsCompleted || _done) return;
            _done = true;
            _onDone?.Invoke();
            Close();
        }
    }
}
