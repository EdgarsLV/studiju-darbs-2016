﻿namespace SdCrypto.SdGui
{
    partial class ThreadedLoadingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lMessage = new System.Windows.Forms.Label();
            this.lLoading = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lMessage
            // 
            this.lMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lMessage.Location = new System.Drawing.Point(13, 13);
            this.lMessage.Name = "lMessage";
            this.lMessage.Size = new System.Drawing.Size(259, 77);
            this.lMessage.TabIndex = 0;
            this.lMessage.Text = "_message_";
            // 
            // lLoading
            // 
            this.lLoading.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lLoading.Location = new System.Drawing.Point(12, 67);
            this.lLoading.Name = "lLoading";
            this.lLoading.Size = new System.Drawing.Size(260, 23);
            this.lLoading.TabIndex = 1;
            this.lLoading.Text = "_loading_";
            this.lLoading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ThreadedLoadingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 99);
            this.Controls.Add(this.lLoading);
            this.Controls.Add(this.lMessage);
            this.Name = "ThreadedLoadingForm";
            this.Text = "Please wait...";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lMessage;
        private System.Windows.Forms.Label lLoading;
    }
}