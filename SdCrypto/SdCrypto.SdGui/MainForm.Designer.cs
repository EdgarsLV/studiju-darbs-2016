﻿namespace SdCrypto.SdGui
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDoStandardTest = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbCryptoAlgos = new System.Windows.Forms.ListBox();
            this.nIterations = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lSelectedCount = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbFiles = new System.Windows.Forms.ListBox();
            this.chkInMemory = new System.Windows.Forms.CheckBox();
            this.btnOpenTestsFolder = new System.Windows.Forms.Button();
            this.btnDeleteTests = new System.Windows.Forms.Button();
            this.btnInitPredefFiles = new System.Windows.Forms.Button();
            this.btnRefreshFileList = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nIterations)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDoStandardTest
            // 
            this.btnDoStandardTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDoStandardTest.Location = new System.Drawing.Point(13, 224);
            this.btnDoStandardTest.Name = "btnDoStandardTest";
            this.btnDoStandardTest.Size = new System.Drawing.Size(259, 23);
            this.btnDoStandardTest.TabIndex = 1;
            this.btnDoStandardTest.Text = "Perform test";
            this.btnDoStandardTest.UseVisualStyleBackColor = true;
            this.btnDoStandardTest.Click += new System.EventHandler(this.btnDoStandardTest_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lbCryptoAlgos);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(259, 178);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Crypt Algo List";
            // 
            // lbCryptoAlgos
            // 
            this.lbCryptoAlgos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCryptoAlgos.FormattingEnabled = true;
            this.lbCryptoAlgos.Location = new System.Drawing.Point(6, 19);
            this.lbCryptoAlgos.Name = "lbCryptoAlgos";
            this.lbCryptoAlgos.Size = new System.Drawing.Size(247, 147);
            this.lbCryptoAlgos.TabIndex = 0;
            this.lbCryptoAlgos.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lbCryptoAlgos_DrawItem);
            // 
            // nIterations
            // 
            this.nIterations.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.nIterations.Location = new System.Drawing.Point(140, 196);
            this.nIterations.Name = "nIterations";
            this.nIterations.Size = new System.Drawing.Size(44, 20);
            this.nIterations.TabIndex = 3;
            this.nIterations.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(90, 199);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Iterations";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 199);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Selected";
            // 
            // lSelectedCount
            // 
            this.lSelectedCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lSelectedCount.AutoSize = true;
            this.lSelectedCount.Location = new System.Drawing.Point(63, 199);
            this.lSelectedCount.Name = "lSelectedCount";
            this.lSelectedCount.Size = new System.Drawing.Size(13, 13);
            this.lSelectedCount.TabIndex = 6;
            this.lSelectedCount.Text = "0";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.lbFiles);
            this.groupBox2.Location = new System.Drawing.Point(278, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(258, 178);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Files";
            // 
            // lbFiles
            // 
            this.lbFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbFiles.FormattingEnabled = true;
            this.lbFiles.Location = new System.Drawing.Point(7, 20);
            this.lbFiles.Name = "lbFiles";
            this.lbFiles.Size = new System.Drawing.Size(245, 147);
            this.lbFiles.TabIndex = 0;
            // 
            // chkInMemory
            // 
            this.chkInMemory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkInMemory.AutoSize = true;
            this.chkInMemory.Location = new System.Drawing.Point(200, 199);
            this.chkInMemory.Name = "chkInMemory";
            this.chkInMemory.Size = new System.Drawing.Size(75, 17);
            this.chkInMemory.TabIndex = 8;
            this.chkInMemory.Text = "In Memory";
            this.chkInMemory.UseVisualStyleBackColor = true;
            // 
            // btnOpenTestsFolder
            // 
            this.btnOpenTestsFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenTestsFolder.Location = new System.Drawing.Point(285, 223);
            this.btnOpenTestsFolder.Name = "btnOpenTestsFolder";
            this.btnOpenTestsFolder.Size = new System.Drawing.Size(121, 23);
            this.btnOpenTestsFolder.TabIndex = 9;
            this.btnOpenTestsFolder.Text = "Open tests folder";
            this.btnOpenTestsFolder.UseVisualStyleBackColor = true;
            this.btnOpenTestsFolder.Click += new System.EventHandler(this.btnOpenTestsFolder_Click);
            // 
            // btnDeleteTests
            // 
            this.btnDeleteTests.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteTests.Location = new System.Drawing.Point(412, 223);
            this.btnDeleteTests.Name = "btnDeleteTests";
            this.btnDeleteTests.Size = new System.Drawing.Size(118, 23);
            this.btnDeleteTests.TabIndex = 10;
            this.btnDeleteTests.Text = "Delete temp tests";
            this.btnDeleteTests.UseVisualStyleBackColor = true;
            this.btnDeleteTests.Click += new System.EventHandler(this.btnDeleteTests_Click);
            // 
            // btnInitPredefFiles
            // 
            this.btnInitPredefFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInitPredefFiles.Location = new System.Drawing.Point(412, 194);
            this.btnInitPredefFiles.Name = "btnInitPredefFiles";
            this.btnInitPredefFiles.Size = new System.Drawing.Size(118, 23);
            this.btnInitPredefFiles.TabIndex = 11;
            this.btnInitPredefFiles.Text = "Init predefined files";
            this.btnInitPredefFiles.UseVisualStyleBackColor = true;
            this.btnInitPredefFiles.Click += new System.EventHandler(this.btnInitPredefFiles_Click);
            // 
            // btnRefreshFileList
            // 
            this.btnRefreshFileList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefreshFileList.Location = new System.Drawing.Point(285, 194);
            this.btnRefreshFileList.Name = "btnRefreshFileList";
            this.btnRefreshFileList.Size = new System.Drawing.Size(121, 23);
            this.btnRefreshFileList.TabIndex = 12;
            this.btnRefreshFileList.Text = "Refresh file list";
            this.btnRefreshFileList.UseVisualStyleBackColor = true;
            this.btnRefreshFileList.Click += new System.EventHandler(this.btnRefreshFileList_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 261);
            this.Controls.Add(this.btnRefreshFileList);
            this.Controls.Add(this.btnInitPredefFiles);
            this.Controls.Add(this.btnDeleteTests);
            this.Controls.Add(this.btnOpenTestsFolder);
            this.Controls.Add(this.chkInMemory);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lSelectedCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nIterations);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnDoStandardTest);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(564, 1000);
            this.MinimumSize = new System.Drawing.Size(564, 0);
            this.Name = "MainForm";
            this.Text = "SdCrypto GUI App";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nIterations)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnDoStandardTest;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lbCryptoAlgos;
        private System.Windows.Forms.NumericUpDown nIterations;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lSelectedCount;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox lbFiles;
        private System.Windows.Forms.CheckBox chkInMemory;
        private System.Windows.Forms.Button btnOpenTestsFolder;
        private System.Windows.Forms.Button btnDeleteTests;
        private System.Windows.Forms.Button btnInitPredefFiles;
        private System.Windows.Forms.Button btnRefreshFileList;
    }
}

